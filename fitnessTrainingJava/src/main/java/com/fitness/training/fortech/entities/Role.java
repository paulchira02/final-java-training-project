package com.fitness.training.fortech.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Model class for Role.
 * <p>
 * Created by Chira Paul on 10/15/2016.
 */
@Entity
@Table(name = "ROLE")
public class Role {

    @Id
    @Column(name = "idRole")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idRole;

    @Column(name = "role")
    private String role;

    @OneToMany(mappedBy = "role")
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int roleId) {
        this.idRole = idRole;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Role{" +
                "idRole=" + idRole +
                ", role='" + role + '\'' +
                '}';
    }
}

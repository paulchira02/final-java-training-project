package com.fitness.training.fortech.controller;

import com.fitness.training.fortech.Validator.ClasaValidator;
import com.fitness.training.fortech.entities.Clasa;
import com.fitness.training.fortech.entities.Client;
import com.fitness.training.fortech.services.ClasaService;
import com.fitness.training.fortech.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chira Paul on 11/3/2016.
 */
@Controller
@RequestMapping("/")
public class ClasaController {
    private ClasaService clasaService;
    private ClientService clientService;


    @Autowired
    @Qualifier("clasaValidator")
    private ClasaValidator validator;

    @InitBinder("clasa")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @Autowired(required = true)
    @Qualifier(value = "ClientService")
    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }


    @Autowired(required = true)
    @Qualifier(value = "ClasaService")
    public void setClasaService(ClasaService clasaService) {
        this.clasaService = clasaService;
    }

    @RequestMapping(value = "/fitness/clasaList/{idClient}", method = RequestMethod.GET)
    public String retrieveClasa(Model model, @PathVariable int idClient) {
        Client client = clientService.getClient(idClient);
        model.addAttribute("client", client);
        model.addAttribute("clasa", new Clasa());
        model.addAttribute("retrieveClasa", this.clasaService.retrieveClases());
        return "programari";
    }

    @RequestMapping(value = "/fitness/clases", method = RequestMethod.GET)
    public String retrieveAllClasa(Model model) {
        model.addAttribute("clasa", new Clasa());
        model.addAttribute("retrieveClasa", this.clasaService.retrieveClases());
        return "clases";
    }

    @RequestMapping(value = "/fitness/saveClas", method = RequestMethod.GET)
    public ModelAndView saveClasForm() {
        ModelAndView modelAndView = new ModelAndView("addClas");
        modelAndView.addObject("clasa", new Clasa());
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/saveClas", method = RequestMethod.POST)
    public ModelAndView addClass(Model model, @ModelAttribute @Validated Clasa clasa, BindingResult result) {

        if (clasa.getIdClasa() != 0) {
            int id = clasa.getIdClasa();
            if (clasaService.getClasa(id) != null)
                result.rejectValue("id", "not.unique.id");
        }

        if (result.hasErrors())
            return new ModelAndView("addClas");

        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/clases");
        this.clasaService.addClasa(clasa);

        String message = "Clasa was successfully added.";
        modelAndView.addObject("message", message);
        return modelAndView;

    }

    @RequestMapping(value = "/fitness/deleteClass/{idClass}", method = RequestMethod.GET)
    public ModelAndView deleteClasa(@PathVariable int idClass) {
        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/clases");
        clasaService.deleteClasa(idClass);
        String message = "Class was successfully deleted.";
        modelAndView.addObject("message", message);
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/editClasa/{idClasa}", method = RequestMethod.GET)
    public ModelAndView updateClasa(@PathVariable int idClasa) {
        ModelAndView modelAndView = new ModelAndView("updateClass");
        Clasa clasa = clasaService.getClasa(idClasa);
        modelAndView.addObject("clasa", clasa);
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/editClasa/{idClasa}", method = RequestMethod.POST)
    public ModelAndView updateClasa(@ModelAttribute("clasa") @Validated Clasa clasa, BindingResult result) {

        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/clases");

        if (result.hasErrors()) {
            modelAndView.setViewName("updateClass");
            modelAndView.addObject("idClasa", clasa.getIdClasa());
            return modelAndView;

        }
        clasaService.updateClasa(clasa);

        String message = "Class was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }
}

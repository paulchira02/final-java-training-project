package com.fitness.training.fortech.dao;

import com.fitness.training.fortech.entities.Clasa;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Chira Paul on 11/3/2016.
 */
@Repository
public class ClasaDaoImpl implements ClasaDao {
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    @SuppressWarnings("unchecked")
    public List<Clasa> retrieveClases() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Clasa> clases = session.createQuery("from Clasa").list();

        for (Clasa c : clases) {
            logger.info("---------------User: " + c.getIdClasa() + "," + c.getTrainer_name() + "," + c.getClasa_name() + "," + c.getSeats());
        }
        return clases;
    }

    public Clasa getClasa(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Clasa clasa = (Clasa) session.get(Clasa.class, id);
        return clasa;
    }

    public void updateClasa(Clasa c) {
        Session session = this.sessionFactory.getCurrentSession();
        Clasa clasaUpd = this.getClasa(c.getIdClasa());
        clasaUpd.setClasa_name(c.getClasa_name());
        clasaUpd.setSeats(c.getSeats());
        clasaUpd.setClients(c.getClients());
        clasaUpd.setTrainer_name(c.getTrainer_name());

        session.update(clasaUpd);
    }

    public void addClasa(Clasa clasa) {
        Session session = this.sessionFactory.getCurrentSession();
        session.merge(clasa);
    }

    public void deleteClasa(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Clasa clasa = getClasa(id);
        if (clasa != null)
            session.delete(clasa);
    }

}

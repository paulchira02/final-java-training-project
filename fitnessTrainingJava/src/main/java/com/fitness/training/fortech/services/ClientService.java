package com.fitness.training.fortech.services;

import com.fitness.training.fortech.entities.Clasa;
import com.fitness.training.fortech.entities.Client;

import java.util.List;
import java.util.Set;

/**
 * Created by Chira Paul on 10/31/2016.
 */
public interface ClientService {
    List<Client> retrieveClient();

    Set<Clasa> retrieveClasesFromClient(int id);

    void updateClient(Client c);

    Client getClient(int id);

    void addClasa(Clasa clasa, int idClient);

    void deleteClientClasa(Clasa clasa, int idClient);

    void addClient(Client client);
}

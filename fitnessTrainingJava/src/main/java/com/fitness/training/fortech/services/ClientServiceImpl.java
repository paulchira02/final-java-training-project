package com.fitness.training.fortech.services;

import com.fitness.training.fortech.dao.ClientDao;
import com.fitness.training.fortech.entities.Clasa;
import com.fitness.training.fortech.entities.Client;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Created by Chira Paul on 10/31/2016.
 */
@Service
@Transactional
public class ClientServiceImpl implements ClientService {
    private ClientDao clientDao;

    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    public List<Client> retrieveClient() {
        return clientDao.retrieveClient();
    }

    public void updateClient(Client c) {
        clientDao.updateClient(c);
    }

    public Client getClient(int id) {
        return clientDao.getClient(id);
    }

    public void addClasa(Clasa clasa, int idClient) {
        clientDao.addClasa(clasa, idClient);
    }

    public Set<Clasa> retrieveClasesFromClient(int id) {
        return clientDao.retrieveClasesFromClient(id);
    }

    public void deleteClientClasa(Clasa clasa, int idClient) {
        clientDao.deleteClientClasa(clasa, idClient);
    }

    public void addClient(Client client) {
        clientDao.addClient(client);
    }
}

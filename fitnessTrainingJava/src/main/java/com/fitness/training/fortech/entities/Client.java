package com.fitness.training.fortech.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Chira Paul on 10/31/2016.
 */
@Entity
@Table(name = "client")
public class Client {

    @Id
    @Column(name = "idClient")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idClient;

    @OneToOne
    @JoinColumn(name = "idUser")
    private User user;

    @Column(name = "cnp")
    private String cnp;

    @Column(name = "adresa")
    private String adresa;

    //@ManyToMany(fetch = FetchType.LAZY, mappedBy = "clients", cascade = CascadeType.ALL)
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "clasa_client",
            joinColumns = {@JoinColumn(name = "idClient")},
            inverseJoinColumns = {@JoinColumn(name = "idClasa")})
    private Set<Clasa> clases = new HashSet<Clasa>();

    public Set<Clasa> getClases() {
        return clases;
    }

    public void setClases(Set<Clasa> clases) {
        this.clases = clases;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getAdresa() {
        return adresa;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void addClasa(Clasa clasa) {
        this.clases.add(clasa);
    }
}

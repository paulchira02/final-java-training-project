package com.fitness.training.fortech.dao;

import com.fitness.training.fortech.entities.Clasa;
import com.fitness.training.fortech.entities.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Chira Paul on 10/31/2016.
 */
@Repository
public class ClientDaoImpl implements ClientDao {
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    @SuppressWarnings("unchecked")
    public List<Client> retrieveClient() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Client> clients = session.createQuery("from Client").list();

        for (Client c : clients) {
            logger.info("---------------User: " + c.getUser().getIdUser() + "," + c.getUser().getUsername() + "," + c.getUser().getPassword() + "," + c.getUser().getFirst_name() + "," + c.getUser().getLast_name() + "," + c.getUser().getRole().getRole() + "," + c.getCnp() + "," + c.getAdresa());
        }
        return clients;
    }

    public Client getClient(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Client client = (Client) session.get(Client.class, id);
        /*String username = client.getUser().getUsername();
        String first_name = client.getUser().getFirst_name();
        String last_name = client.getUser().getLast_name();
        String password = client.getUser().getPassword();
        String adresa = client.getAdresa();
        String role = client.getUser().getRole().getRole();
        String cnp = client.getCnp();*/
        return client;
    }

    public void updateClient(Client c) {
        Session session = this.sessionFactory.getCurrentSession();
        Client clientUpd = this.getClient(c.getIdClient());
        clientUpd.setAdresa(c.getAdresa());
        clientUpd.setCnp(c.getCnp());
        clientUpd.setUser(c.getUser());
        //
        /*String username = c.getUser().getUsername();
        String first_name = c.getUser().getFirst_name();
        String last_name = c.getUser().getLast_name();
        String password = c.getUser().getPassword();
        String adresa = c.getAdresa();*/
        //String role = c.getUser().getRole().getRole();
        String cnp = c.getCnp();
        //clientUpd.setUser(c.getUser());
        session.update(clientUpd);
    }

    public void addClasa(Clasa clasa, int idClient) {
        Session session = this.sessionFactory.getCurrentSession();
        Client c = this.getClient(idClient);
        //clasa.getClients().add(c);
        c.addClasa(clasa);
        session.merge(c);
        int id = c.getIdClient();
    }

    public Set<Clasa> retrieveClasesFromClient(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Client client = this.getClient(id);
        return client.getClases();
    }

    public void deleteClientClasa(Clasa clasa, int idClient) {
        Session session = this.sessionFactory.getCurrentSession();
        Client client = this.getClient(idClient);
        Set<Clasa> clases = client.getClases();
        Iterator<Clasa> it = clases.iterator();
        while (it.hasNext()) {
            Clasa s = it.next();
            if (s.getIdClasa() == clasa.getIdClasa()) {
                it.remove();
            }
        }
        client.setClases(clases);
        session.update(client);
    }

    public void addClient(Client client) {
        Session session = this.sessionFactory.getCurrentSession();
        session.merge(client);
    }
}

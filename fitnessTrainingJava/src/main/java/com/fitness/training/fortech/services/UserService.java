package com.fitness.training.fortech.services;

import com.fitness.training.fortech.entities.User;

import java.util.List;

/**
 * Created by Chira Paul on 10/16/2016.
 */
public interface UserService {
    List<User> retrieveUser();

    void addUser(User u);

    User getUser(int id);

    void deleteUser(int id);

    void updateUser(User u);
}

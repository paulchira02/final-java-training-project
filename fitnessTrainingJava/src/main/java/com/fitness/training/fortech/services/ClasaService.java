package com.fitness.training.fortech.services;

import com.fitness.training.fortech.entities.Clasa;

import java.util.List;

/**
 * Created by Chira Paul on 11/3/2016.
 */
public interface ClasaService {
    List<Clasa> retrieveClases();

    Clasa getClasa(int id);

    void updateClasa(Clasa c);

    void addClasa(Clasa clasa);

    void deleteClasa(int id);

}

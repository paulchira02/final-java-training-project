package com.fitness.training.fortech.dao;


import com.fitness.training.fortech.entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by Chira Paul on 10/16/2016.
 */

@Repository
public class UserDaoImpl implements UserDao {
    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    @SuppressWarnings("unchecked")
    public List<User> retrieveUser() {
        Session session = this.sessionFactory.getCurrentSession();
        List<User> users = session.createQuery("from User").list();

        for (User u : users) {
            logger.info("----------------------------User: " + u.getIdUser() + "," + u.getUsername() + "," + u.getPassword() + "," + u.getFirst_name() + "," + u.getLast_name() + "," + u.getRole().getRole());
        }
        return users;
    }

    public void addUser(User u) {
        Session session = this.sessionFactory.getCurrentSession();
        int id = u.getIdUser();
        session.merge(u);
        logger.info("User saved " + u);
    }

    public User getUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, id);
        return user;
    }

    public void deleteUser(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = getUser(id);
        if (user != null)
            session.delete(user);
    }

    public void updateUser(User u) {
        Session session = this.sessionFactory.getCurrentSession();
        User userUpd = this.getUser(u.getIdUser());
        userUpd.setUsername(u.getUsername());
        userUpd.setPassword(u.getPassword());
        userUpd.setFirst_name(u.getFirst_name());
        userUpd.setLast_name(u.getLast_name());
        userUpd.setRole(u.getRole());
        userUpd.setEmail(u.getEmail());
        session.update(userUpd);
    }
}

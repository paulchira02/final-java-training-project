package com.fitness.training.fortech.services;


import com.fitness.training.fortech.dao.UserDao;
import com.fitness.training.fortech.entities.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by Chira Paul on 10/16/2016.
 */

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }


    public List<User> retrieveUser() {
        return userDao.retrieveUser();
    }

    public void addUser(User u) {
        this.userDao.addUser(u);
    }

    public User getUser(int id) {
        return userDao.getUser(id);
    }

    public void deleteUser(int id) {
        this.userDao.deleteUser(id);
    }

    public void updateUser(User u) {
        this.userDao.updateUser(u);
    }
}

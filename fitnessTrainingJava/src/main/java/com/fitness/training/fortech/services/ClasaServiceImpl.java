package com.fitness.training.fortech.services;

import com.fitness.training.fortech.dao.ClasaDao;
import com.fitness.training.fortech.entities.Clasa;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Chira Paul on 11/3/2016.
 */
@Service
@Transactional
public class ClasaServiceImpl implements ClasaService {
    private ClasaDao clasaDao;

    public void setClasaDao(ClasaDao clasaDao) {
        this.clasaDao = clasaDao;
    }

    public List<Clasa> retrieveClases() {
        return clasaDao.retrieveClases();
    }

    public Clasa getClasa(int id) {
        return clasaDao.getClasa(id);
    }

    public void updateClasa(Clasa c) {
        clasaDao.updateClasa(c);
    }

    public void addClasa(Clasa clasa) {
        clasaDao.addClasa(clasa);
    }

    public void deleteClasa(int id) {
        clasaDao.deleteClasa(id);
    }
}

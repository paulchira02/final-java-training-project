package com.fitness.training.fortech.dao;

import com.fitness.training.fortech.entities.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Chira Paul on 10/15/2016.
 */
@Repository
public class RoleDaoImpl implements RoleDao {

    private static final Logger logger = LoggerFactory.getLogger(RoleDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public Role getRole(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Role role = (Role) session.get(Role.class, id);
        return role;
    }

    public List<Role> retrieveRoles() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Role> roles = session.createQuery("from Role").list();

        for (Role r : roles) {
            logger.info("----------------------Role: " + r.getIdRole() + "," + r.getRole());
        }

        return roles;
    }

    public void addRole(Role r) {

    }
}

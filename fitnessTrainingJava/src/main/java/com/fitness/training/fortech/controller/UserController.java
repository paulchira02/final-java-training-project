package com.fitness.training.fortech.controller;

import com.fitness.training.fortech.Validator.UserValidator;
import com.fitness.training.fortech.entities.Role;
import com.fitness.training.fortech.entities.User;
import com.fitness.training.fortech.services.RoleService;
import com.fitness.training.fortech.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chira Paul on 10/26/2016.
 */

@Controller
@RequestMapping("/")
public class UserController {
    private UserService userService;
    private RoleService roleService;

    @Autowired
    @Qualifier("userValidator")
    private UserValidator validator;

    @InitBinder("user")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @Autowired(required = true)
    @Qualifier(value = "UserService")
    public void setUserService(UserService ps) {
        this.userService = ps;
    }

    @Autowired(required = true)
    @Qualifier(value = "RoleService")
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping(value = "/fitness/usersList", method = RequestMethod.GET)
    public String retrieveUser(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("retrieveUser", this.userService.retrieveUser());
        return "user";
    }

    @RequestMapping(value = "/fitness//home/admin", method = RequestMethod.GET)
    public String retrieveAdminHome(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("retrieveUser", this.userService.retrieveUser());
        return "adminHome";
    }

    @RequestMapping(value = "/fitness/addUser", method = RequestMethod.GET)
    public ModelAndView showUserForm() {
        ModelAndView modelAndView = new ModelAndView("addUser");

        modelAndView.addObject("user", new User());

        return modelAndView;
    }

    @RequestMapping(value = "/fitness/addUser", method = RequestMethod.POST)
    public ModelAndView addUser(Model model, @ModelAttribute @Validated User u, BindingResult result, @RequestParam String idrole) {

        if (u.getIdUser() != 0) {
            int id = u.getIdUser();
            if (userService.getUser(id) != null)
                result.rejectValue("id", "not.unique.id");
        }

        int myflag = 0;
        for (Role r : roleService.retrieveRoles()) {
            if (r.getRole().equals(idrole)) {
                u.setRole(r);
                myflag += 1;
                break;
            }
        }
        if (myflag == 0) {
            result.rejectValue("role", "Role doesn't exist!!", new Object[]{"'role'"}, "Role doesn't exist!!");
        }
        if (result.hasErrors())
            return new ModelAndView("addUser");

        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/usersList");
        //Role r = roleService.getRole(3);
        //u.setRole(r);

        this.userService.addUser(u);

        String message = "User was successfully added.";
        modelAndView.addObject("message", message);
        return modelAndView;

    }

    @RequestMapping(value = "/fitness/delete/{idUser}", method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable int idUser) {
        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/usersList");
        userService.deleteUser(idUser);
        String message = "User was successfully deleted.";
        modelAndView.addObject("message", message);
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/edit/{idUser}", method = RequestMethod.GET)
    public ModelAndView updateUser(@PathVariable int idUser) {
        ModelAndView modelAndView = new ModelAndView("updateUser");
        User user = userService.getUser(idUser);
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/edit/{idUser}", method = RequestMethod.POST)
    public ModelAndView updateUser(@ModelAttribute("user") @Validated User user, BindingResult result, @PathVariable int idUser, @RequestParam String idrole) {

        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/usersList");
        user.setIdUser(idUser);
        int myflag = 0;
        for (Role r : roleService.retrieveRoles()) {
            if (r.getRole().equals(idrole)) {
                user.setRole(r);
                myflag += 1;
                break;
            }
        }

        if (myflag == 0) {
            result.rejectValue("role", "Role doesn't exist!!", new Object[]{"'role'"}, "Role doesn't exist!!");
        }

        if (result.hasErrors()) {
            ModelAndView model = new ModelAndView("updateUser");
            model.addObject("idUser", idUser);
            return model;

        }
        userService.updateUser(user);

        String message = "User was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }
}

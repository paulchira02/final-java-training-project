package com.fitness.training.fortech.Validator;

import com.fitness.training.fortech.entities.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Chira Paul on 11/7/2016.
 */
public class UserValidator implements Validator {

    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "required.username", "Username is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "first_name", "required.first_name", "First name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "last_name", "required.last_name", "Last name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required.password", "Password is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required.email", "Email is required.");
        User user = (User) o;
        if (user.getEmail().contains("@") == false) {
            errors.rejectValue("email", "bad value", new Object[]{"'email'"}, "enter valid email!");
        }
    }
}

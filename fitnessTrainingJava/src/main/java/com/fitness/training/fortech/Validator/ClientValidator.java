package com.fitness.training.fortech.Validator;

import com.fitness.training.fortech.entities.Client;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


/**
 * Created by Chira Paul on 11/7/2016.
 */
public class ClientValidator implements Validator {
    public boolean supports(Class<?> aClass) {
        return Client.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {
        Client client = (Client) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.username", "required.user.username", "Field name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.first_name", "required.user.first_name", "First name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.last_name", "required.user.last_name", "Last name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.password", "required.user.password", "Password is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "adresa", "required.adresa", "Adresa is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cnp", "required.cnp", "CNP is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.email", "required.user.email", "Email is required.");


        String cnp = client.getCnp();
        if (cnp.length() < 13 || cnp.length() > 13) {
            errors.rejectValue("cnp", "bad value", new Object[]{"'cnp'"}, "cnp length 13!!");
        }
        if (cnp.matches(".*[^0-9]+.*") && cnp.length() > 0) {
            errors.rejectValue("cnp", "bad value", new Object[]{"'cnp'"}, "cnp must have only digits!!");
        }
        String first_name = client.getUser().getFirst_name();
        String last_name = client.getUser().getLast_name();
        if (first_name.matches(".*[0-9]+.*")) {
            errors.rejectValue("user.first_name", "bad value", new Object[]{"'user.first_name'"}, "first_name cannot contain numbers!!");
        }
        if (last_name.matches(".*[0-9]+.*")) {
            errors.rejectValue("user.last_name", "bad value", new Object[]{"'user.last_name'"}, "last_name cannot contain numbers!!");
        }
        String email = client.getUser().getEmail();

        if (email.contains("@") == false) {
            errors.rejectValue("user.email", "bad value", new Object[]{"'user.email'"}, "enter a valid email!");
        }
    }
}

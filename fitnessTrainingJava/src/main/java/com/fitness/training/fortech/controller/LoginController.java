package com.fitness.training.fortech.controller;

import com.fitness.training.fortech.Validator.UserRegistrationValidator;
import com.fitness.training.fortech.entities.Client;
import com.fitness.training.fortech.entities.User;
import com.fitness.training.fortech.services.ClientService;
import com.fitness.training.fortech.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chira Paul on 10/27/2016.
 */
@Controller
@RequestMapping("/")
public class LoginController {

    @Autowired(required = true)
    @Qualifier(value = "UserService")
    private UserService userService;

    @Autowired(required = true)
    @Qualifier(value = "ClientService")
    private ClientService clientService;

    @Autowired
    @Qualifier("userRegistrationValidator")
    private UserRegistrationValidator validator;

    @InitBinder("user")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    public static User loggedUser = new User();

    public static User getLoggedUser() {
        return loggedUser;
    }

    public static void setLoggedUser(User loggedUser) {
        LoginController.loggedUser = loggedUser;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView loginAttempt() {
        ModelAndView modelAndView = new ModelAndView("login");

        modelAndView.addObject("user", new User());

        return modelAndView;
    }

    @SuppressWarnings("null")
    @RequestMapping(value = "/fitness/authenticate", method = RequestMethod.POST)
    public ModelAndView loginForm(@ModelAttribute("user") @Validated User user, BindingResult result) {

        int flag = 0;
        for (User u : userService.retrieveUser()) {
            if (u.getUsername().equals(user.getUsername())) {
                flag += 1;
                break;
            }
        }

        if (result.getErrorCount() == 0 && flag == 0) {
            result.rejectValue("username", "Username doesn't exist!!", new Object[]{"'username'"}, "Username doesn't exist!!");
        }

        if (result.hasErrors()) {
            return new ModelAndView("login");
        }

        String message = "";
        ModelAndView modelAndView = null;

        for (User toCheck : userService.retrieveUser()) {
            if (toCheck.getUsername().equals(user.getUsername()) && toCheck.getPassword().equals(user.getPassword())) {
                if (toCheck.getRole().getIdRole() == 1) {
                    modelAndView = new ModelAndView("adminHome");
                    message = "Login successful";
                    modelAndView.addObject("message", message);
                    modelAndView.addObject("user", toCheck);
                } else if (toCheck.getRole().getIdRole() == 3) {
                    modelAndView = new ModelAndView("client");
                    message = "Login successful";
                    modelAndView.addObject("message", message);
                    Client cc;
                    for (Client c : clientService.retrieveClient()) {
                        if (c.getUser().getIdUser() == toCheck.getIdUser()) {
                            cc = c;
                            modelAndView.addObject("client", cc);
                        }
                    }

                }
                modelAndView.addObject("retrieveUser", this.userService.retrieveUser());
                return modelAndView;

            }
        }

        modelAndView = new ModelAndView("redirect:/loginFailed");
        message = "Login not successful";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value = "/fitness/logout", method = RequestMethod.GET)
    public ModelAndView logout() {

        return this.loginAttempt();

    }
}

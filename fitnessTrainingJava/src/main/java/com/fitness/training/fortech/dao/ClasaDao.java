package com.fitness.training.fortech.dao;

import com.fitness.training.fortech.entities.Clasa;

import java.util.List;

/**
 * Created by Chira Paul on 11/3/2016.
 */
public interface ClasaDao {
    List<Clasa> retrieveClases();

    Clasa getClasa(int id);

    void updateClasa(Clasa c);

    void addClasa(Clasa clasa);

    void deleteClasa(int id);
}

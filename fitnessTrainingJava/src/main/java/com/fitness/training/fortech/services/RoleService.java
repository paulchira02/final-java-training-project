package com.fitness.training.fortech.services;


import com.fitness.training.fortech.entities.Role;

import java.util.List;

/**
 * Created by Chira Paul on 10/15/2016.
 */
public interface RoleService {

    Role getRole(int id);

    List<Role> retrieveRoles();
}

package com.fitness.training.fortech.controller;

import com.fitness.training.fortech.Validator.ClientValidator;
import com.fitness.training.fortech.entities.Clasa;
import com.fitness.training.fortech.entities.Client;
import com.fitness.training.fortech.entities.Role;
import com.fitness.training.fortech.entities.User;
import com.fitness.training.fortech.services.ClasaService;
import com.fitness.training.fortech.services.ClientService;
import com.fitness.training.fortech.services.RoleService;
import com.fitness.training.fortech.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Chira Paul on 10/26/2016.
 */

@Controller
@RequestMapping("/")
public class ClientController {
    private UserService userService;
    private ClientService clientService;
    private RoleService roleService;
    private ClasaService clasaService;

    @Autowired
    @Qualifier("clientValidator")
    private ClientValidator validator;

    @InitBinder("client")
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @Autowired(required = true)
    @Qualifier(value = "RoleService")
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired(required = true)
    @Qualifier(value = "UserService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired(required = true)
    @Qualifier(value = "ClientService")
    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    @Autowired(required = true)
    @Qualifier(value = "ClasaService")
    public void setClasaService(ClasaService clasaService) {
        this.clasaService = clasaService;
    }


    @RequestMapping(value = "/fitness/clientList", method = RequestMethod.GET)
    public String retrieveClient(Model model) {
        model.addAttribute("client", new Client());
        model.addAttribute("retrieveClient", this.clientService.retrieveClient());
        return "client2";
    }

    @RequestMapping(value = "/fitness/home/{idClient}", method = RequestMethod.GET)
    public String retrieveCurrentClient(@PathVariable int idClient, Model model) {
        Client client = clientService.getClient(idClient);
        model.addAttribute("client", client);
        return "client";
    }

    @RequestMapping(value = "/fitness/appointments/{idClient}", method = RequestMethod.GET)
    public String retrieveCurrentClientClases(@PathVariable int idClient, Model model) {
        Client client = clientService.getClient(idClient);
        model.addAttribute("client", client);
        model.addAttribute("clasa", new Clasa());
        model.addAttribute("retireveClientClases", this.clientService.retrieveClasesFromClient(idClient));
        return "clientAppointments";
    }

    @RequestMapping(value = "/fitness/editClient/{idClient}", method = RequestMethod.GET)
    public ModelAndView updateClient(@PathVariable int idClient) {
        ModelAndView modelAndView = new ModelAndView("updateClient");
        Client client = clientService.getClient(idClient);
        modelAndView.addObject("client", client);
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/editClient/{idClient}", method = RequestMethod.POST)
    public ModelAndView updateClient(@ModelAttribute("client") @Validated Client client, BindingResult result, @PathVariable int idClient) {

        String usern = client.getUser().getUsername();
        for (User u : userService.retrieveUser()) {
            if ((u.getUsername().equals(client.getUser().getUsername()) && u.getUsername().equals(usern) == false)) {
                result.rejectValue("user.username", "Username must be unique!!", new Object[]{"'user.username'"}, "Username exists!!");
                break;
            }
        }

        if (result.hasErrors()) {
            ModelAndView model = new ModelAndView("updateClient");
            model.addObject("idClient", client.getIdClient());
            return model;
        }

        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/editClient/{idClient}");
        /*String username = request.getParameter("username");
        String first_name = request.getParameter("first_name");
        String last_name = request.getParameter("last_name");
        String password = request.getParameter("password");*/

        //int userid = client.getUser().getIdUser();
        client.setIdClient(idClient);
        String username = client.getUser().getUsername();
        int idc = idClient;
        String first_name = client.getUser().getFirst_name();
        String last_name = client.getUser().getLast_name();
        String password = client.getUser().getPassword();
        //String rolee = client.getUser().getRole();
        int idu = client.getUser().getIdUser();
        //client.setIdClient(idc);
        //User u = client.getUser();
        Role role = roleService.getRole(3);
        //User user = userService.getUser(client.getUser().getIdUser());
        //user.setRole(role);
        //client.setUser(user);
        client.getUser().setRole(role);
        userService.updateUser(client.getUser());
        //user.setRole(role);
        //user.setUsername(username);
        //user.setFirst_name(first_name);
        //user.setLast_name(last_name);
        //user.setPassword(password);
        //client.setUser(user);
        clientService.updateClient(client);

        String message = "Client was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value = "/fitness/addClasa/{idClasa}", method = RequestMethod.GET)
    public ModelAndView addClasaM(@PathVariable int idClasa, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();

        Clasa clasa = clasaService.getClasa(idClasa);
        modelAndView.addObject("clasa", clasa);

        int seats = clasa.getSeats();

        String id = request.getParameter("idClient");
        int idClient = Integer.parseInt(id);

        int flag_clasaExist = 0;
        for (Clasa c : clientService.retrieveClasesFromClient(idClient)) {
            if (c.getIdClasa() == idClasa) {
                flag_clasaExist += 1;
                break;
            }
        }
        String errors = "not error";
        if (seats == 0) {
            errors = "nu mai sunt locuri";
        }
        if (flag_clasaExist == 1) {
            errors = "ai programare";
        }
        if (errors.equals("not error") == false) {
            modelAndView.setViewName("redirect:/fitness/clasaList/{idClient}");
            modelAndView.addObject("idClient", idClient);
            modelAndView.addObject("error", errors);
            modelAndView.addObject("idClasa", idClasa);
            return modelAndView;
        }
        seats = seats - 1;
        clasa.setSeats(seats);


        modelAndView.setViewName("redirect:/fitness/appointments/{idClient}");
        modelAndView.addObject("idClient", idClient);

        //try{

        clientService.addClasa(clasa, Integer.parseInt(id));
        clasaService.updateClasa(clasa);
        //}catch (IllegalStateException e){

        //}
        String message = "Programare was successfully edited.";
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping(value = "/fitness/deleteClientClasa/{idClasa}", method = RequestMethod.GET)
    public ModelAndView deleteClientClasa(@PathVariable int idClasa, HttpServletRequest request) {
        String id = request.getParameter("idClient");
        int idClient = Integer.parseInt(id);
        ModelAndView modelAndView = new ModelAndView("redirect:/fitness/appointments/{idClient}");
        modelAndView.addObject("idClient", idClient);
        Clasa clasa = this.clasaService.getClasa(idClasa);
        String nume = clasa.getClasa_name();
        clasa.setSeats(clasa.getSeats() + 1);
        clasaService.updateClasa(clasa);
        clientService.deleteClientClasa(clasa, Integer.parseInt(id));
        String message = "Class from client was successfully deleted.";
        modelAndView.addObject("message", message);
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/registerClient", method = RequestMethod.GET)
    public ModelAndView registerClient() {
        ModelAndView modelAndView = new ModelAndView("registerClient");
        modelAndView.addObject("client", new Client());
        return modelAndView;
    }

    @RequestMapping(value = "/fitness/registerClient", method = RequestMethod.POST)
    public ModelAndView registerClient(Model model, @ModelAttribute @Validated Client client, BindingResult result) {

        if (client.getIdClient() != 0) {
            int id = client.getIdClient();
            if (clientService.getClient(id) != null)
                result.rejectValue("id", "not.unique.id");
        }
        for (User u : userService.retrieveUser()) {
            if (u.getUsername().equals(client.getUser().getUsername())) {
                result.rejectValue("user.username", "Username must be unique!!", new Object[]{"'user.username'"}, "Username exists!!");
                break;
            }
        }
        if (result.hasErrors())
            return new ModelAndView("registerClient");

        ModelAndView modelAndView = new ModelAndView("redirect:/");
        String username = client.getUser().getUsername();
        String fname = client.getUser().getFirst_name();
        String lname = client.getUser().getLast_name();
        String passw = client.getUser().getPassword();
        String adresa = client.getAdresa();
        int id = client.getUser().getIdUser();
        String cnp = client.getCnp();
        User user = client.getUser();
        int idd = client.getIdClient();
        Role r = roleService.getRole(3);
        user.setRole(r);
        client.setUser(user);
        //u.setRole(r);
        /*for (Role r : roleService.retrieveRoles()) {
            if (r.getRole().equals(idrole)) {
                u.setRole(r);
                break;
            }
        }
        this.userService.addUser(u);
        */
        userService.addUser(user);
        for (User u : userService.retrieveUser()) {
            if (u.getUsername().equals(client.getUser().getUsername())) {
                int idUser = u.getIdUser();
                User useradd = userService.getUser(idUser);
                client.setUser(useradd);
                break;
            }
        }
        clientService.addClient(client);
        String message = "User was successfully added.";
        modelAndView.addObject("message", message);
        return modelAndView;

    }
}

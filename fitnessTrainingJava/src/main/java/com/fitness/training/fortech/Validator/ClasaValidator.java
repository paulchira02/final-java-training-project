package com.fitness.training.fortech.Validator;

import com.fitness.training.fortech.entities.Clasa;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Chira Paul on 11/7/2016.
 */
public class ClasaValidator implements Validator {

    public boolean supports(Class<?> aClass) {
        return Clasa.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "clasa_name", "required.clasa_name", "clasa_name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "trainer_name", "required.trainer_name", "trainer_name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "seats", "required.seats", "Seats is required.");
    }
}

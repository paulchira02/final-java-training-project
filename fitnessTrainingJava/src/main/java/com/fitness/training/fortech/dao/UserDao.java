package com.fitness.training.fortech.dao;


import com.fitness.training.fortech.entities.User;

import java.util.List;

/**
 * Created by Chira Paul on 10/16/2016.
 */
public interface UserDao {
    List<User> retrieveUser();

    void addUser(User u);

    void deleteUser(int id);

    User getUser(int id);

    void updateUser(User u);
}

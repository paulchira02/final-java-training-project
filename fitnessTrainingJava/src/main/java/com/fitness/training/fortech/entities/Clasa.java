package com.fitness.training.fortech.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Chira Paul on 11/2/2016.
 */
@Entity
@Table(name = "clasa")
public class Clasa {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idClasa")
    private int idClasa;

    @Column(name = "clasa_name")
    private String clasa_name;

    @Column(name = "trainer_name")
    private String trainer_name;

    @Column(name = "seats")
    private int seats;

    /*@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "clasa_client",
            joinColumns = {@JoinColumn(name = "idClasa")},
            inverseJoinColumns = {@JoinColumn(name = "idClient")})*/
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "clases", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Client> clients = new HashSet<Client>();

    public Set<Client> getClients() {
        return clients;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    public int getIdClasa() {
        return idClasa;
    }

    public void setIdClasa(int idClasa) {
        this.idClasa = idClasa;
    }

    public String getClasa_name() {
        return clasa_name;
    }

    public void setClasa_name(String clasa_name) {
        this.clasa_name = clasa_name;
    }

    public String getTrainer_name() {
        return trainer_name;
    }

    public void setTrainer_name(String trainer_name) {
        this.trainer_name = trainer_name;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
}

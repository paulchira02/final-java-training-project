package com.fitness.training.fortech.services;


import com.fitness.training.fortech.dao.RoleDao;
import com.fitness.training.fortech.entities.Role;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by Chira Paul on 10/15/2016.
 */

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    private RoleDao roleDao;

    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public Role getRole(int id) {
        return roleDao.getRole(id);
    }

    public List<Role> retrieveRoles() {
        return roleDao.retrieveRoles();
    }
}

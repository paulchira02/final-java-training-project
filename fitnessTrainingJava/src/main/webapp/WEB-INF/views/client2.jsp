<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="<c:url value="/js/main.js" />"></script>

</head>
<body class="clientBack">
<div id="centralDiv">
    <ul>
        <li><a class="menu" href="/fitness/clientList">Home</a></li>
        <li><a class="menu" href="/fitness/clientList">Personal Date</a></li>
        <li><a class="menu" href="/fitness/editClient/${client.idClient}">Update account</a></li>
        <li><a class="menu" href="/fitness/clientList">Appointments</a></li>
        <li><a class="menu" href="/fitness/logout/">Log Out</a></li>
    </ul>
    <center>
        <h3>User List</h3>
        <c:if test="${!empty retrieveClient}">
        <table class="tg">
            <tr>
                <th width="120">First Name</th>
                <th width="120">Last Name</th>
                <th width="60">Username</th>
                <th width="60">Password</th>
                <th width="60">Role</th>
                <th width="80">Adresa</th>
                <th width="80">CNP</th>
                <th width="80">Email</th>
            </tr>
            <c:forEach items="${retrieveClient}" var="client">
                <tr>
                    <td><c:out value="${client.user.first_name}"/></td>
                    <td><c:out value="${client.user.last_name}"/></td>
                    <td><c:out value="${client.user.username}"/></td>
                    <td><c:out value="${client.user.password}"/></td>
                    <td><c:out value="${client.user.role.role}"/></td>
                    <td><c:out value="${client.adresa}"/></td>
                    <td><c:out value="${client.cnp}"/></td>
                    <td><c:out value="${client.user.email}"/></td>

                </tr>
            </c:forEach>
        </table>
        </c:if>
</div>
</center>
</div>
</body>
</html>
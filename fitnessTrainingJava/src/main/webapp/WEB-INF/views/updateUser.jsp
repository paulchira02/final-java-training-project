<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>fitness Application | Update Form</title>
</head>
<body class="adminBack">
<div id="centralDiv">
    <ul>
        <li><a class="menu" href="/fitness/home/admin">Home</a></li>
        <li><a class="menu" href="/fitness/usersList">Users</a></li>
        <li><a class="menu" href="/fitness/addUser">Add User</a></li>
        <li><a class="menu" href="/fitness/clases">Classes</a></li>
        <li><a class="menu" href="/fitness/logout/">Log Out</a></li>
    </ul>
    <center>

        <br>
        <div class="title">fitness Application | Update Form</div>
        <br>
        <br>

        <h2>Update User</h2>
        <c:if test="${saved == 'success'}">
            <p class="success">User Updated Successfully</p>
        </c:if>
        <form:form method="POST" modelAttribute="user" action="/fitness/edit/${user.idUser}">
            <table width="400px" height="150px">
                <tr>
                    <td>Username</td>
                    <td><form:input path="username"/></td>
                    <td><form:errors path="username" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td><form:input path="first_name"/></td>
                    <td><form:errors path="first_name" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><form:input path="last_name"/></td>
                    <td><form:errors path="last_name" style="color: red;"></form:errors></td>
                </tr>

                <tr>
                    <td>Password</td>
                    <td><form:input path="password"/></td>
                    <td><form:errors path="password" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>Role</td>
                        <%--<td><form:input path="role"/></td>
                        <td><form:errors path="role" style="color: red;"></form:errors></td>--%>
                    <td><input name="idrole" type="text"/>
                    <td>
                    <td><form:errors path="role" style="color: red;"></form:errors></td>

                </tr>
                <tr>
                    <td>Email</td>
                    <td><form:input path="email"/></td>
                    <td><form:errors path="email" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td></td>
                    <td><input class="myButton" type="submit" value="Save"/></td>
                </tr>
            </table>
        </form:form>
    </center>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="<c:url value="/js/main.js" />"></script>

</head>
<body class="clientBack">
<div id="centralDiv">
    <ul>
        <li><a class="menu" href="/fitness/home/${client.idClient}">Home</a></li>
        <li><a class="menu" href="/fitness/appointments/${client.idClient}">My Appointments</a></li>
        <li><a class="menu" href="/fitness/editClient/${client.idClient}">Update account</a></li>
        <li><a class="menu" href="/fitness/clasaList/${client.idClient}">Classes</a></li>
        <li><a class="menu" href="/fitness/logout/">Log Out</a></li>
    </ul>
    <center>
        <h2>Welcome ${client.user.first_name } ${client.user.last_name}</h2>
        <div class="mytable">
            <table>
                <tr>
                    <td>Username</td>
                    <td>Password</td>
                    <td>Role</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Adresa</td>
                    <td>CNP</td>
                    <td>Email</td>
                </tr>
                <tr>
                    <td><c:out value="${client.user.username}"/></td>
                    <td><c:out value="${client.user.password}"/></td>
                    <td><c:out value="${client.user.role.role}"/></td>
                    <td><c:out value="${client.user.first_name}"/></td>
                    <td><c:out value="${client.user.last_name}"/></td>
                    <td><c:out value="${client.adresa}"/></td>
                    <td><c:out value="${client.cnp}"/></td>
                    <td><c:out value="${client.user.email}"/></td>
                    <%--<td><a href="<c:url value='/fitness/edit/${user.idUser}' />" >Edit</a></td>
                        <td><a href="<c:url value='/fitness/delete/${user.idUser}' />" >Delete</a></td>--%>
                <tr>
            </table>
        </div>
    </center>
</div>
</body>
</html>
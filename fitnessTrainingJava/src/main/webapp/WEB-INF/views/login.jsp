<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet" href="<c:url value="/css/style.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>fitness</title>
</head>
<body class="loginBack">

<div id="centralDiv">

    <img class="image"
         src="<c:url value="/images/logo.jpg"/>"/>
    <center>
        <div class="border">
            <h2>Log In</h2>
            <br>

            <form action="/fitness/registerClient">
                <input type="submit" value="Register" width="48" height="48"/>
            </form>
            <br><br>
            <form:form name="loginForm" commandName="user" action="/fitness/authenticate/" method="POST">
                <table class="tableLog">
                    <tr>
                        <td>Username</td>
                        <td><form:input path="username"/></td>
                        <td><form:errors path="username" style="color: red;"></form:errors></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><form:password path="password"/></td>
                        <td><form:errors path="password" style="color: red;"></form:errors></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td><input class="myButton" type="submit" value="Log In"/></td>
                    </tr>

                </table>
            </form:form>
        </div>
    </center>
</div>

</body>
</html>
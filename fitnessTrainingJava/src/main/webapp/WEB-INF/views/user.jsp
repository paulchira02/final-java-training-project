<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="<c:url value="/js/main.js" />"></script>
    <title>fitness</title>
</head>
<body class="adminBack">
<div id="centralDiv">
    <ul>
        <li><a class="menu" href="/fitness/home/admin">Home</a></li>
        <li><a class="menu" href="/fitness/usersList">Users</a></li>
        <li><a class="menu" href="/fitness/addUser">Add User</a></li>
        <li><a class="menu" href="/fitness/clases">Classes</a></li>
        <li><a class="menu" href="/fitness/logout/">Log Out</a></li>
    </ul>
    <center>
        <div class="mytable">
            <table>
                <tr>
                    <td>Id</td>
                    <td>Username</td>
                    <td>Role</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Password</td>
                    <td>Email</td>
                </tr>

                <c:forEach var="user" items="${retrieveUser}">
                    <tr>
                        <td><c:out value="${user.idUser}"/></td>
                        <td><c:out value="${user.username}"/></td>
                        <td><c:out value="${user.role.role}"/></td>
                        <td><c:out value="${user.first_name}"/></td>
                        <td><c:out value="${user.last_name}"/></td>
                        <td><c:out value="${user.password}"/></td>
                        <td><c:out value="${user.email}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </center>
</div>
</body>
</html>
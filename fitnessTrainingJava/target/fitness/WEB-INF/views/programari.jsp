<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="<c:url value="/js/main.js" />"></script>
    <title>fitness</title>
</head>
<body class="adminBack">
<div id="centralDiv">
    <ul>
        <li><a class="menu" href="/fitness/home/${client.idClient}">Home</a></li>
        <li><a class="menu" href="/fitness/appointments/${client.idClient}">My Appointments</a></li>
        <li><a class="menu" href="/fitness/editClient/${client.idClient}">Update account</a></li>
        <li><a class="menu" href="/fitness/clasaList/${client.idClient}">Classes</a></li>
        <li><a class="menu" href="/fitness/logout/">Log Out</a></li>
    </ul>
    <center>
        <div class="mytable">
            <c:set var="errors" scope="session" value='<%= request.getParameter("error")%>'/>
            <c:set var="idc" scope="session" value='<%= request.getParameter("idClasa")%>'/>
            <table>
                <tr>
                    <td>Id</td>
                    <td>Clasa</td>
                    <td>Antrenor</td>
                    <td>Locuri</td>
                </tr>
                <c:forEach var="clasa" items="${retrieveClasa}">
                    <tr>
                        <td><c:out value="${clasa.idClasa}"/></td>
                        <td><c:out value="${clasa.clasa_name}"/></td>
                        <td><c:out value="${clasa.trainer_name}"/></td>
                        <td><c:out value="${clasa.seats}"/></td>
                            <%--<td><a href="<c:url value='/fitness/addClasa/${clasa.idClasa}' />" >Rezerva</a></td>
                            --%>
                        <td>
                            <form action="/fitness/addClasa/${clasa.idClasa}">
                                <input type="hidden" name="idClient" id="idClient" value="${client.idClient}"/>
                                <input type="submit" value="Rezerva"/>
                                <c:if test="${not empty errors && idc == clasa.idClasa}">
                                    <div style="display: inline;color:#FF0000" id="hiddenText">${errors}</div>
                                </c:if>
                                    <%--<div style="display: none;color:#FF0000" id="hiddenText" >This is hidden</div>--%>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </center>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body class="adminBack">
<div id="centralDiv">
    <center>

        <br>
        <div class="title">Register Client</div>
        <br>
        <br>

        <c:if test="${saved == 'success'}">
            <p class="success">New Client!</p>
        </c:if>
        <form:form method="POST" modelAttribute="client" action="/fitness/registerClient">
            <table width="400px" height="150px">
                <tr>
                    <td>Username</td>
                    <td><form:input path="user.username"/></td>
                    <td><form:errors path="user.username" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td><form:input path="user.first_name"/></td>
                    <td><form:errors path="user.first_name" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><form:input path="user.last_name"/></td>
                    <td><form:errors path="user.last_name" style="color: red;"></form:errors></td>
                </tr>

                <tr>
                    <td>Password</td>
                    <td><form:input path="user.password"/></td>
                    <td><form:errors path="user.password" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>Adresa</td>
                    <td><form:input path="adresa"/></td>
                    <td><form:errors path="adresa" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>CNP</td>
                    <td><form:input path="cnp"/></td>
                    <td><form:errors path="cnp" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><form:input path="user.email"/></td>
                    <td><form:errors path="user.email" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td><form:hidden path="user.idUser"></form:hidden></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td></td>
                    <td><input class="myButton" type="submit" value="Save"/></td>

                </tr>

            </table>
        </form:form>
    </center>
</div>
</body>
</html>
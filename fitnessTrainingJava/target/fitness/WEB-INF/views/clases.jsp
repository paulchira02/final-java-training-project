<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="<c:url value="/js/main.js" />"></script>

</head>
<body class="adminBack">
<div id="centralDiv">
    <ul>
        <li><a class="menu" href="/fitness/home/admin">Home</a></li>
        <li><a class="menu" href="/fitness/usersList">Users</a></li>
        <li><a class="menu" href="/fitness/addUser">Add User</a></li>
        <li><a class="menu" href="/fitness/clases">Classes</a></li>
        <li><a class="menu" href="/fitness/logout/">Log Out</a></li>
    </ul>
    <center>
        <div class="mytable">
            <table>
                <tr>
                    <td>ID</td>
                    <td>Clas</td>
                    <td>Trainer</td>
                    <td>Seats</td>
                </tr>

                <c:forEach items="${retrieveClasa}" var="clasa">
                    <tr>
                        <td><c:out value="${clasa.idClasa}"/></td>
                        <td><c:out value="${clasa.clasa_name}"/></td>
                        <td><c:out value="${clasa.trainer_name}"/></td>
                        <td><c:out value="${clasa.seats}"/></td>
                        <td><a href="<c:url value='/fitness/editClasa/${clasa.idClasa}' />">Edit</a></td>
                        <td><a href="<c:url value='/fitness/deleteClass/${clasa.idClasa}' />">Delete</a></td>
                    </tr>
                </c:forEach>
            </table>
            <td>
                <form action="/fitness/saveClas">
                    <input type="submit" value="New Class"/>
                </form>
            </td>
        </div>
    </center>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/style.css" />"/>
    <link type="text/css" rel="stylesheet"
          href="<c:url value="/css/menu.css" />"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body class="adminBack">
<div id="centralDiv">
    <ul>
        <li><a class="menu" href="/fitness/home/admin">Home</a></li>
        <li><a class="menu" href="/fitness/usersList">Users</a></li>
        <li><a class="menu" href="/fitness/addUser">Add User</a></li>
        <li><a class="menu" href="/fitness/clases">Classes</a></li>
        <li><a class="menu" href="/fitness/logout/">Log Out</a></li>
    </ul>
    <center>

        <br>

        <h2>Add Class</h2>
        <c:if test="${saved == 'success'}">
            <p class="success">Class Successfully saved</p>
        </c:if>
        <form:form method="POST" modelAttribute="clasa" action="/fitness/saveClas">
            <table width="400px" height="150px">
                <tr>
                    <td>Clasa</td>
                    <td><form:input path="clasa_name"/></td>
                    <td><form:errors path="clasa_name" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>Trainer</td>
                    <td><form:input path="trainer_name"/></td>
                    <td><form:errors path="trainer_name" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td>Seats</td>
                    <td><form:input path="seats"/></td>
                    <td><form:errors path="seats" style="color: red;"></form:errors></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input class="myButton" type="submit" value="Save"/></td>
                </tr>
            </table>
        </form:form>
    </center>
</div>
</body>
</html>